{ config, pkgs, ... }:
let
  flake-cmd = pkgs.writeShellScriptBin "flake" ''
    flakeFile="$HOME/.dotfiles/flake.nix"
    if [ $(id -u) != 0 ]; then
      cmdPrefix="sudo"
    fi

    function showHelp() {
        cat <<EOF
    Usage:
      flake [command]

    Environment:
      flake file: $flakeFile

    Available commands:
      rb  | rebuild         nixos-rebuild --flake .#$(hostname)
      prb | pull-rebuild    git pull, nixos-rebuild --flake .#$(hostname)
      sw  | switch          nixos-rebuild switch --flake .#$(hostname)
      psw | pull-switch     git pull, nixos-rebuild switch --flake .#$(hostname)
      u   | update          nix flake update
      h   | help            print help

    Flags:
      -h  | --help          print help
    EOF
    }

    function checkFlake() {
        flakeFile="$1"
        if [ ! -r "$flakeFile" ]; then
            echo "Flake file $flakeFile doesn't exist or isn't readable. Exiting."
            exit 1
        fi
    }
    function rebuild() {
        checkFlake "$flakeFile"
        pushd "$(dirname $flakeFile)"
        $cmdPrefix nixos-rebuild build --flake .#"$(hostname)"
        popd
    }

    function pull-rebuild() {
        checkFlake "$flakeFile"
        pushd $(dirname $flakeFile)
        git pull || exit 1
        popd
        rebuild
    }

    function rebuild-switch() {
        checkFlake "$flakeFile"
        pushd $(dirname $flakeFile)
        $cmdPrefix nixos-rebuild switch --flake .#"$(hostname)"
        popd
    }

    function pull-rebuild-switch() {
        checkFlake "$flakeFile"
        pushd $(dirname $flakeFile)
        git pull || exit 1
        popd
        rebuild-switch
    }

    function update() {
        checkFlake "$flakeFile"
        pushd "$(dirname $flakeFile)"
        nix flake update
        popd
    }

    command="$1"

    case "$command" in
        "rb" | "rebuild" )
            rebuild
            ;;
        "prb" | "pull-rebuild" )
            pull-rebuild
            ;;
        "sw" | "switch" )
            rebuild-switch
            ;;
        "psw" | "pull-switch" )
            pull-rebuild-switch
            ;;
        "u" | "update" )
            update
            ;;
        "h" | "help" | "-h" | "--help" )
            showHelp
            ;;
        * )
            echo "Error: unknown command $command"
            showHelp
            exit 1
            ;;
    esac
  '';
in
{
  # Make ready for nix flakes
  nix.package = pkgs.nixFlakes;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  documentation.man.enable = true;

  environment.systemPackages = with pkgs;[
    vim toybox bat ripgrep fd
    wget curl git
    home-manager
    flake-cmd
  ];

  time.timeZone = "Europe/Paris";
}
