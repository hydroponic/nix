{ lib, pkgs, hostsPath, ... }:
with builtins;
let
  sys = "x86_64-linux";
in {
  mkVps = { vpsName,
           system ? sys, ... }:
    lib.nixosSystem {
      inherit system;
      specialArgs = { inherit lib system; };
      modules = [
        ../modules
        {
          networking.hostName = vpsName;
        }
        (import "${hostsPath}/vps/${vpsName}/configuration.nix")
      ];
    };
}
