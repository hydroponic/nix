{
  description = "Modules, lib collection";
  outputs = { self }:
  {
    myLib = import ./lib;
    myMod = import ./modules;
  };
}
